package pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;

public class pomfirst {

    //locators
    public static WebElement username(WebDriver driver) {
        return driver.findElement(By.xpath("//input[@placeholder='Email']"));

    }

    public static WebElement password(WebDriver driver) {
        return driver.findElement(By.name("password"));

    }
    public static WebElement enterlogin(WebDriver driver) {
       return driver.findElement(By.xpath("//button[@class='btn login-btn']"));

    }
}