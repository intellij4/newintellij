package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class LoginTest {


    public static void main(String[] args) throws InterruptedException {


        System.setProperty("webdriver.edge.driver", "C:\\Users\\STS-W10-2022-02\\Downloads\\edge driver\\msedgedriver.exe");
        //  WebDriverManager.chromedriver().setup();
        WebDriver driver = new EdgeDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://shenll-hrms.web.app/login");
        PageFactory.initElements(driver,LoginpageObjects.class);
        PageFactory.initElements(driver,dashboardpage.class);
        LoginpageObjects.username.sendKeys("admin@shenll.com");
        LoginpageObjects.password.sendKeys("Test@123");
        LoginpageObjects.login.click();
        Thread.sleep(8000);
        dashboardpage.employee.click();
        Thread.sleep(2000);
        dashboardpage.add.click();
        Thread.sleep(2000);
        dashboardpage.name.sendKeys("meenakshi");
    }}