package pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class LoginwithoutFindBy {
    @FindBy(xpath ="//input[@placeholder='Email']")
    public static WebElement username;
    public static WebElement password;

    @FindBy(xpath ="//button[@class='btn login-btn']" )
    public static WebElement login;

    public static void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.edge.driver", "C:\\Users\\STS-W10-2022-02\\Downloads\\edge driver\\msedgedriver.exe");
        // WebDriverManager.chromedriver().setup();
        WebDriver driver = new EdgeDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://shenll-hrms.web.app/login");
        PageFactory.initElements(driver,LoginwithoutFindBy.class);
        LoginwithoutFindBy.username.sendKeys("admin@shenll.com");
        password.sendKeys("Test@123");
        login.click();
        }
    }
