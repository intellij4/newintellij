package PageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RoleObjects {
     @FindBy(xpath="//span[text()=' Manage Employee ']")
     public static WebElement Employeemodule;
     @FindBy(xpath = "(//mat-icon[text()='more_vert'])[1]")
    public static WebElement action;
     @FindBy(xpath="//span[text()='View']")
    public static WebElement view;

    @FindBy(id="mat-select-value-63")
    public static WebElement role;

    @FindBy(linkText = "mat-select-value-63")
    public static WebElement userrole;

}
