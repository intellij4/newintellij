package PageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EmployeeLeaveObjects {
    @FindBy(xpath="//span[text()=' Manage Leave ']")
    public static WebElement Leavemodule;
    @FindBy(linkText = "22")
    public static WebElement Remainingleave;
    @FindBy(xpath="(//td[text()=' 22 '])[1]")
    public static WebElement RemainingLeave;
}
