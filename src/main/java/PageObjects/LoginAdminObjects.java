package PageObjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginAdminObjects {
    @FindBy(xpath ="//input[@placeholder='Email']")
    public static WebElement username;
    @FindBy(name="password")
    public static WebElement password;
    @FindBy(xpath ="//button[@class='btn login-btn']" )
    public static WebElement login;
}


