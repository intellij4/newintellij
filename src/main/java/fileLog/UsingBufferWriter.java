package fileLog;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class UsingBufferWriter {
    public static void main(String[] args) throws IOException {
        String location="UsingBufferWriter.txt";
        String content="Learning Write java file";
        FileWriter fileWriter=new FileWriter(location);
        BufferedWriter bufferedWriter=new BufferedWriter(fileWriter);
       bufferedWriter.write(content);
      bufferedWriter.close();

    }
}
