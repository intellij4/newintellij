package CommonFunction;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.annotations.BeforeSuite;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

public class Utils{
    public static WebDriver driver=null;
   public static Properties  properties=null;
    public Properties loadpropertyFile() throws IOException {
        FileInputStream fileInputStream=new FileInputStream("config properties");
         properties=new Properties();
        properties.load(fileInputStream);
        return properties;
    }
    @BeforeSuite
    public void launchBrowser() throws IOException {

        loadpropertyFile();
        String browser = properties.getProperty("browser");
        String url = properties.getProperty("url");
        String location = properties.getProperty("location");
        browser.equalsIgnoreCase("edge") ;
            System.setProperty("WebDriver.edge.driver",location);
            WebDriver driver = new EdgeDriver();

        /* else if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", location);
            WebDriver driver = new ChromeDriver();
        */
        driver.manage().window().maximize();
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }
public void teardown(){
      //  driver.quit();

}
}
